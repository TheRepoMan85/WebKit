layer at (0,0) size 785x737
  RenderView at (0,0) size 785x600
layer at (0,0) size 785x737
  RenderBlock {HTML} at (0,0) size 785x737
    RenderBody {BODY} at (8,8) size 769x721 [bgcolor=#CCCCCC]
      RenderBlock {P} at (0,0) size 769x20
        RenderText {#text} at (0,0) size 355x19
          text run at (0,0) width 355: "The style declarations which apply to the text below are:"
      RenderBlock {PRE} at (0,36) size 769x96
        RenderText {#text} at (0,0) size 200x96
          text run at (0,0) width 136: "A {color: green;}"
          text run at (136,0) width 0: " "
          text run at (0,16) width 184: "A:link {color: purple;}"
          text run at (184,16) width 0: " "
          text run at (0,32) width 192: "A:visited {color: lime;}"
          text run at (192,32) width 0: " "
          text run at (0,48) width 200: "A:active {color: maroon;}"
          text run at (200,48) width 0: " "
          text run at (0,64) width 176: "#one {color: #006600;}"
          text run at (176,64) width 0: " "
          text run at (0,80) width 0: " "
      RenderBlock {P} at (0,163) size 769x20
        RenderText {#text} at (0,0) size 500x19
          text run at (0,0) width 500: "The following anchors should appear as described; none of them should be red."
      RenderBlock {UL} at (0,199) size 769x160
        RenderListItem {LI} at (40,0) size 729x120
          RenderBlock (anonymous) at (0,0) size 729x20
            RenderListMarker at (-18,0) size 7x19: bullet
            RenderText {#text} at (0,0) size 567x19
              text run at (0,0) width 567: "Purple unvisited, lime (light green) visited, maroon (dark red) while active (being clicked):"
          RenderBlock {UL} at (0,20) size 729x100
            RenderListItem {LI} at (40,0) size 689x20
              RenderListMarker at (-18,0) size 7x19: white bullet
              RenderInline {A} at (0,0) size 109x19 [color=#800080]
                RenderText {#text} at (0,0) size 109x19
                  text run at (0,0) width 109: "W3C Web server"
              RenderText {#text} at (0,0) size 0x0
            RenderListItem {LI} at (40,20) size 689x20
              RenderListMarker at (-18,0) size 7x19: white bullet
              RenderInline {A} at (0,0) size 111x19 [color=#800080]
                RenderText {#text} at (0,0) size 111x19
                  text run at (0,0) width 111: "NIST Web server"
              RenderText {#text} at (0,0) size 0x0
            RenderListItem {LI} at (40,40) size 689x20
              RenderListMarker at (-18,0) size 7x19: white bullet
              RenderInline {A} at (0,0) size 124x19 [color=#800080]
                RenderText {#text} at (0,0) size 124x19
                  text run at (0,0) width 124: "CWRU Web server"
              RenderText {#text} at (0,0) size 0x0
            RenderListItem {LI} at (40,60) size 689x20
              RenderListMarker at (-18,0) size 7x19: white bullet
              RenderInline {A} at (0,0) size 46x19 [color=#800080]
                RenderText {#text} at (0,0) size 46x19
                  text run at (0,0) width 46: "Yahoo!"
              RenderText {#text} at (0,0) size 0x0
            RenderListItem {LI} at (40,80) size 689x20
              RenderListMarker at (-18,0) size 7x19: white bullet
              RenderInline {A} at (0,0) size 58x19 [color=#800080]
                RenderText {#text} at (0,0) size 58x19
                  text run at (0,0) width 58: "Erewhon"
              RenderText {#text} at (58,0) size 219x19
                text run at (58,0) width 219: " (don't click on it, it goes nowhere)"
        RenderListItem {LI} at (40,120) size 729x40
          RenderBlock (anonymous) at (0,0) size 729x20
            RenderListMarker at (-18,0) size 7x19: bullet
            RenderText {#text} at (0,0) size 204x19
              text run at (0,0) width 204: "Dark green in any circumstance:"
          RenderBlock {UL} at (0,20) size 729x20
            RenderListItem {LI} at (40,0) size 689x20
              RenderListMarker at (-18,0) size 7x19: white bullet
              RenderInline {A} at (0,0) size 124x19 [color=#006600]
                RenderText {#text} at (0,0) size 124x19
                  text run at (0,0) width 124: "CWRU Web server"
              RenderText {#text} at (0,0) size 0x0
      RenderBlock {P} at (0,375) size 769x40
        RenderText {#text} at (0,0) size 118x19
          text run at (0,0) width 118: "The quoted word \""
        RenderInline {A} at (0,0) size 43x19 [color=#008000]
          RenderText {#text} at (118,0) size 43x19
            text run at (118,0) width 43: "anchor"
        RenderText {#text} at (161,0) size 729x39
          text run at (161,0) width 376: "\" should be green, NOT purple, since it's part of an anchor. "
          text run at (537,0) width 192: "It's a named anchor, and styles"
          text run at (0,20) width 349: "declared for the A tag are applied to them under CSS1. "
          text run at (349,20) width 326: "It also should NOT turn orange when clicked upon."
      RenderTable {TABLE} at (0,431) size 769x290 [border: (1px outset #808080)]
        RenderTableSection {TBODY} at (1,1) size 767x288
          RenderTableRow {TR} at (0,0) size 767x28
            RenderTableCell {TD} at (0,0) size 767x28 [bgcolor=#C0C0C0] [border: (1px inset #808080)] [r=0 c=0 rs=1 cs=2]
              RenderInline {STRONG} at (0,0) size 160x19
                RenderText {#text} at (4,4) size 160x19
                  text run at (4,4) width 160: "TABLE Testing Section"
          RenderTableRow {TR} at (0,28) size 767x260
            RenderTableCell {TD} at (0,144) size 12x28 [bgcolor=#C0C0C0] [border: (1px inset #808080)] [r=1 c=0 rs=1 cs=1]
              RenderText {#text} at (4,4) size 4x19
                text run at (4,4) width 4: " "
            RenderTableCell {TD} at (12,28) size 755x260 [border: (1px inset #808080)] [r=1 c=1 rs=1 cs=1]
              RenderBlock {P} at (4,4) size 747x20
                RenderText {#text} at (0,0) size 500x19
                  text run at (0,0) width 500: "The following anchors should appear as described; none of them should be red."
              RenderBlock {UL} at (4,40) size 747x160
                RenderListItem {LI} at (40,0) size 707x120
                  RenderBlock (anonymous) at (0,0) size 707x20
                    RenderListMarker at (-18,0) size 7x19: bullet
                    RenderText {#text} at (0,0) size 567x19
                      text run at (0,0) width 567: "Purple unvisited, lime (light green) visited, maroon (dark red) while active (being clicked):"
                  RenderBlock {UL} at (0,20) size 707x100
                    RenderListItem {LI} at (40,0) size 667x20
                      RenderListMarker at (-18,0) size 7x19: white bullet
                      RenderInline {A} at (0,0) size 109x19 [color=#800080]
                        RenderText {#text} at (0,0) size 109x19
                          text run at (0,0) width 109: "W3C Web server"
                      RenderText {#text} at (0,0) size 0x0
                    RenderListItem {LI} at (40,20) size 667x20
                      RenderListMarker at (-18,0) size 7x19: white bullet
                      RenderInline {A} at (0,0) size 111x19 [color=#800080]
                        RenderText {#text} at (0,0) size 111x19
                          text run at (0,0) width 111: "NIST Web server"
                      RenderText {#text} at (0,0) size 0x0
                    RenderListItem {LI} at (40,40) size 667x20
                      RenderListMarker at (-18,0) size 7x19: white bullet
                      RenderInline {A} at (0,0) size 124x19 [color=#800080]
                        RenderText {#text} at (0,0) size 124x19
                          text run at (0,0) width 124: "CWRU Web server"
                      RenderText {#text} at (0,0) size 0x0
                    RenderListItem {LI} at (40,60) size 667x20
                      RenderListMarker at (-18,0) size 7x19: white bullet
                      RenderInline {A} at (0,0) size 46x19 [color=#800080]
                        RenderText {#text} at (0,0) size 46x19
                          text run at (0,0) width 46: "Yahoo!"
                      RenderText {#text} at (0,0) size 0x0
                    RenderListItem {LI} at (40,80) size 667x20
                      RenderListMarker at (-18,0) size 7x19: white bullet
                      RenderInline {A} at (0,0) size 58x19 [color=#800080]
                        RenderText {#text} at (0,0) size 58x19
                          text run at (0,0) width 58: "Erewhon"
                      RenderText {#text} at (58,0) size 219x19
                        text run at (58,0) width 219: " (don't click on it, it goes nowhere)"
                RenderListItem {LI} at (40,120) size 707x40
                  RenderBlock (anonymous) at (0,0) size 707x20
                    RenderListMarker at (-18,0) size 7x19: bullet
                    RenderText {#text} at (0,0) size 204x19
                      text run at (0,0) width 204: "Dark green in any circumstance:"
                  RenderBlock {UL} at (0,20) size 707x20
                    RenderListItem {LI} at (40,0) size 667x20
                      RenderListMarker at (-18,0) size 7x19: white bullet
                      RenderInline {A} at (0,0) size 124x19 [color=#006600]
                        RenderText {#text} at (0,0) size 124x19
                          text run at (0,0) width 124: "CWRU Web server"
                      RenderText {#text} at (0,0) size 0x0
              RenderBlock {P} at (4,216) size 747x40
                RenderText {#text} at (0,0) size 118x19
                  text run at (0,0) width 118: "The quoted word \""
                RenderInline {A} at (0,0) size 43x19 [color=#008000]
                  RenderText {#text} at (118,0) size 43x19
                    text run at (118,0) width 43: "anchor"
                RenderText {#text} at (161,0) size 729x39
                  text run at (161,0) width 376: "\" should be green, NOT purple, since it's part of an anchor. "
                  text run at (537,0) width 192: "It's a named anchor, and styles"
                  text run at (0,20) width 349: "declared for the A tag are applied to them under CSS1. "
                  text run at (349,20) width 326: "It also should NOT turn orange when clicked upon."
layer at (8,153) size 769x2 clip at (0,0) size 0x0
  RenderBlock {HR} at (0,145) size 769x2 [border: (1px inset #000000)]
