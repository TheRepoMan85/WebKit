layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {P} at (0,0) size 784x20
        RenderText {#text} at (0,0) size 249x19
          text run at (0,0) width 249: "What it should look like (positive case):"
      RenderBlock {DIV} at (0,36) size 784x76 [border: (1px solid #008000)]
        RenderBlock {DIV} at (1,11) size 782x22 [border: (1px solid #0000FF)]
          RenderText {#text} at (1,1) size 84x19
            text run at (1,1) width 84: "Lorem ipsum"
        RenderBlock {DIV} at (1,43) size 782x22 [border: (1px dotted #0000FF)]
          RenderText {#text} at (1,1) size 84x19
            text run at (1,1) width 84: "Lorem ipsum"
      RenderBlock {P} at (0,128) size 784x20
        RenderText {#text} at (0,0) size 253x19
          text run at (0,0) width 253: "What it should look like (negative case):"
      RenderBlock {DIV} at (0,164) size 784x36 [border: (1px solid #008000)]
        RenderBlock {DIV} at (1,11) size 782x22 [border: (1px solid #0000FF)]
          RenderText {#text} at (1,1) size 84x19
            text run at (1,1) width 84: "Lorem ipsum"
        RenderBlock {DIV} at (1,23) size 782x22 [border: (1px dotted #0000FF)]
          RenderText {#text} at (1,1) size 84x19
            text run at (1,1) width 84: "Lorem ipsum"
      RenderBlock {P} at (0,216) size 784x20
        RenderText {#text} at (0,0) size 371x19
          text run at (0,0) width 371: "Dynamic case (automatically testing positive --> negative):"
      RenderBlock {DIV} at (0,252) size 784x36 [border: (1px solid #008000)]
        RenderBlock {DIV} at (1,11) size 782x22 [border: (1px solid #0000FF)]
          RenderText {#text} at (1,1) size 84x19
            text run at (1,1) width 84: "Lorem ipsum"
        RenderBlock {DIV} at (1,23) size 782x22 [border: (1px dotted #0000FF)]
          RenderText {#text} at (1,1) size 84x19
            text run at (1,1) width 84: "Lorem ipsum"
      RenderBlock (anonymous) at (0,288) size 784x45
        RenderBR {BR} at (0,0) size 0x19
        RenderButton {INPUT} at (0,20) size 109x25 [color=#000000CC] [bgcolor=#C0C0C0] [border: (2px outset #C0C0C0)]
          RenderBlock (anonymous) at (8,4) size 93x16
            RenderText at (0,0) size 93x16
              text run at (0,0) width 93: "Negative margin"
        RenderText {#text} at (109,22) size 4x19
          text run at (109,22) width 4: " "
        RenderButton {INPUT} at (113,20) size 105x25 [color=#000000CC] [bgcolor=#C0C0C0] [border: (2px outset #C0C0C0)]
          RenderBlock (anonymous) at (8,4) size 89x16
            RenderText at (0,0) size 89x16
              text run at (0,0) width 89: "Positive margin"
        RenderText {#text} at (0,0) size 0x0
      RenderBlock {P} at (0,349) size 784x20
        RenderText {#text} at (0,0) size 449x19
          text run at (0,0) width 449: "Dynamic case (automatically testing positive --> negative --> positive):"
      RenderBlock {DIV} at (0,385) size 784x76 [border: (1px solid #008000)]
        RenderBlock {DIV} at (1,11) size 782x22 [border: (1px solid #0000FF)]
          RenderText {#text} at (1,1) size 84x19
            text run at (1,1) width 84: "Lorem ipsum"
        RenderBlock {DIV} at (1,43) size 782x22 [border: (1px dotted #0000FF)]
          RenderText {#text} at (1,1) size 84x19
            text run at (1,1) width 84: "Lorem ipsum"
      RenderBlock (anonymous) at (0,461) size 784x45
        RenderBR {BR} at (0,0) size 0x19
        RenderButton {INPUT} at (0,20) size 109x25 [color=#000000CC] [bgcolor=#C0C0C0] [border: (2px outset #C0C0C0)]
          RenderBlock (anonymous) at (8,4) size 93x16
            RenderText at (0,0) size 93x16
              text run at (0,0) width 93: "Negative margin"
        RenderText {#text} at (109,22) size 4x19
          text run at (109,22) width 4: " "
        RenderButton {INPUT} at (113,20) size 105x25 [color=#000000CC] [bgcolor=#C0C0C0] [border: (2px outset #C0C0C0)]
          RenderBlock (anonymous) at (8,4) size 89x16
            RenderText at (0,0) size 89x16
              text run at (0,0) width 89: "Positive margin"
        RenderText {#text} at (0,0) size 0x0
